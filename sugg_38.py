import copy


class Pizza(object):
    def __init__(self, name, size, price):
        self.name = name
        self.size = size
        self.price = price

    def getPizzaInfo(self):
        return self.name, self.size, self.price

    def showPizzaInfo(self):
        print("pizza name: ", self.name)
        print("pizza size: ", self.size)
        print("pizza price: ", self.price)

    def changeSize(self, size):
        self.size = size

    def changePrice(self, price):
        self.price = price


class Order(object):
    def __init__(self, name):
        self.customer_name = name
        self.pizza_list = []
        self.pizza_list.append(Pizza("Mushroom", 12, 30))

    def orderMore(self, pizza):
        self.pizza_list.append(pizza)

    def changeName(self, name):
        self.customer_name = name

    def getOrderDetail(self):
        print("customer name: ", self.customer_name)
        for i in self.pizza_list:
            i.showPizzaInfo()

    def getPizza(self, number):
        return self.pizza_list[number]


if __name__ == "__main__":
    customer1 = Order("zhang")
    customer1.orderMore(Pizza("seafood", 9, 40))
    customer1.orderMore(Pizza("fruit", 12, 45))
    print("customer1 order information: ")
    customer1.getOrderDetail()
    print("-------------------------------------------------")

    customer2 = copy.copy(customer1)  # 浅拷贝
    customer2 = copy.deepcopy(customer1)  # 深拷贝
    customer2.changeName("Li")
    customer2.getPizza(2).changeSize(31)
    customer2.getPizza(2).changePrice(100)
    print("customer2 order information: ")
    customer2.getOrderDetail()
    print("-------------------------------------------------")
    print("customer1 order information: ")
    customer1.getOrderDetail()
    print("-------------------------------------------------")
