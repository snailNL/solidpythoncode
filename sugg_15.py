# 使用enumerate()获取序列迭代的索引和值
my_list = ["a", "b", "c", "d", "e"]

for index, value in enumerate(my_list):
    print("index: ", index, "value: ", value)

'''
# enumerate()函数的内部实现
def enumerate(sequence, start=0):
    n = start
    for elem in sequence:
        yield n, elem
        n += 1
'''

# 定义自己的迭代器，使其以倒序的方式获得序列的索引和值


def my_enumerate(sequence):
    n = -1
    for elem in reversed(sequence):
        yield len(sequence)+n, elem
        n = n - 1


for index, value in my_enumerate(my_list):
    print("index: ", index, "value: ", value)


# 字典的迭代使用iteritems()
presoninfo = {"name": "John", "age": 20, "hobby": "football"}
for k, v in presoninfo.items():
    print (k, ": ", v)
