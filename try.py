def waitTime(args):
    values = [int(i) for i in args.split(" ")]
    add_sum = 0
    add_num = 0
    minus_num = 0
    for value in values:
        if value >= 0:
            add_sum += value
            add_num += 1
        else:
            minus_num += 1
    result1 = minus_num
    result2 = round(float(add_sum)/float(add_num),1)
    return result1, result2


if __name__ == "__main__":
    x = "5 1 2 3 4 5"
    x, y = waitTime(x)
    print(x, y)
