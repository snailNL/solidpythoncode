import threading, time, sys
import time



class test(threading.Thread):

    def __init__(self, name, delay):
        threading.Thread.__init__(self)
        self.name = name
        self.delay = delay

    def run(self):
        print("%s delay for %s" %(self.name, self.delay))
        time.sleep(self.delay)
        c = 0
        while True:
            print("This is thread  %s on line %s" %(self.name, c))
            c = c + 1
            if c == 3:
                print("End of thread %s" %(self.name))
                break

def myfunc(a, delay):
    print("I will calculate square of %s after delay for %s "%(a, delay))
    time.sleep(delay)
    print("calculate begins...")
    result = a*a
    print(result)
    return result


if __name__=="__main__":
    '''
    t1 = test("Thread 1", 2)
    t2 = test("Thread 2",  2)
    t1.start()
    print("wait t1 to end")
    t1.join()
    t2.start()
    print("end of main")
    '''
    t1 = threading.Thread(target=myfunc, args=(2, 5))
    t2 = threading.Thread(target=myfunc, args=(6, 8))
    print(t1.isDaemon())
    print(t2.isDaemon())
    # t2.setDaemon(True)
    t1.start()
    t2.start()
