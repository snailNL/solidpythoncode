class A:
    def __nonzero__(self):
        print("testing A.nonzero__()")
        return True

    def __len__(self):
        print("get length")
        return False

if __name__=="__main__":
    if A():
        print("not empty")
    else:
        print("empty")
