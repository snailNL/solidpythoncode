# 使用Lazy evaluation/延迟计算/惰性计算
# 生成器表达式
# 斐波那契数列

def fib():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a+b
