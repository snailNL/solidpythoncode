import sys
import const


class _const:

    class ConstError(TypeError):
        pass

    class ConstCaseError(ConstError):
        pass

    def __setattr__(self, name, value):
        if self.__dict__.has_key(name):
            raise self.ConstError("Can't change const .%s" % name)
        if not name.isupper():
            raise self.ConstCaseError(
                "const name '%s' is not all uppercase" % name)
        self.__dict__[name] = value


sys.modules[__name__] = _const()


const.MY_CONSTANT = 1
const.MY_SWCOND_CONST = 2

"""
# 在其他模块中引入这些常量时，按照如下方式进行：
# 以上定义常量的代码放置在constant.py 文件中
from constant import const
print(const.MY_CONSTANT)
print(const.MY_SECOND_CONSTANT*2)
"""
