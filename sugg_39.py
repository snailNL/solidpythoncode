from collections import Counter

data = ['a', 2, 3, 'b', 45, 1, 2, 3, 45, 'a']
print(Counter(data))

# use element() method to get dict's key
print(list(Counter(data).elements()))

# use update() to add elements' new value
old_count = Counter(data)
old_count.update("aaaaaaaaaaabbbbbbbbbb")
print("new_count: ", old_count)

# use subtract to minus
old_count.subtract("aaaaabbbb")
print("minus_count: ", old_count)
