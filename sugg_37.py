from operator import itemgetter
from timeit import Timer

phonebook = {"Linda": "7750", "Bob": "9345", "carl": "5834"}
sorted_pb = sorted(phonebook.iteritems(), key=itemgetter(1))
print("sorted phonebook", sorted_pb)


print(Timer(stmt="sorted(xs, key=lambda x:x[1])", setup="xs=range(100); xs=zip(xs, xs);").timeit(10000))
