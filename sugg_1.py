# quicksort implementation
def quicksort(array):
    less = []
    greater = []
    if len(array) <= 1:
        return array
    pivot = array.pop()
    for x in array:
        if x <= pivot:
            less.append(x)
        else:
            greater.append(x)
    return quicksort(less)+[pivot]+quicksort(greater)

# exchange two objects
# a, b = b, a


if __name__ == "__main__":
    array = [7, 3, 6, 8, 324, 5, 2, 45, 564, 33, 543, 1, 4]
    sorted_array = quicksort(array)
    print(sorted_array)

    # reversed()
    a = [1, 2, 3, 4, 5]
    print(a[::-1])  # normal
    print(list(reversed(a)))  # pythonic

    # 字符串格式化
    print("hello %s! " %("Tom"))  # normal
    print("hello %(name)s!" %{"name": "Tom"})  # pythonic

    # for example, pythonic, 占位符的使用
    value = {"greet": "Hello World", "language": "Python"}
    print("%(greet)s from %(language)s." % value)

    # 不使用占位符时，str.format() is recommended mostly, most pythonic
    print("{greet} from {language}. ".format(greet="Hello world",
                                             language="python"))
