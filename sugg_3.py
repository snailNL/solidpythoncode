# python跳转表


def jump(x):
    return {
            0: "You Typed zero.\n",
            1: "You Typed one.\n",
            2: "You Typed two.\n",
            3: "You Typed three.\n",
            4: "You Typed four.\n",
            5: "You Typed five.\n",
            6: "You Typed six.\n",
            7: "You Typed seven.\n",
            8: "You Typed eight.\n",
            9: "You Typed nine.\n"
    }.get(x, "Only single-digit numbers are allowed\n")


if __name__ == "__main__":
    print(jump(3))
