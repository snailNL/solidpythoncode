class MyContextManager(object):
    def __enter__(self):
        print("entering...")
    def __exit__(self, exception_type, exception_value, traceback):
        print("leaving...")
        if exception_type is None:
            print("No exceptions")
            return False
        elif exception_type is ValueError:
            print(" value error...")
            return True
        else:
            print("othre error...")
            return True


if __name__ == "__main__":
    with MyContextManager():
        print("testing...")
        raise(ValueError)
