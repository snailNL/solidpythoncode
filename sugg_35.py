class Fruit(object):
    total = 0
    @classmethod
    def print_total(cls):
        print(cls.total)
        print(id(Fruit.total))
        print(id(cls.total))

    @classmethod
    def set(cls, value):
        print("calling class_method(%s, %s)" % (cls, value))
        cls.total = value


class Apple(Fruit):
    pass


class Orange(Fruit):
    pass


app1 = Apple()
app1.set(200)
app2 = Apple()
org1 = Orange()
org1.set(300)
org2 = Orange()
app1.print_total()
org1.print_total()
